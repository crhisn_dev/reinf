//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.09.24 �s 01:12:14 PM BRT 
//


package br.com.tti.sefaz.reinf.eventos.retorno;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.tti.sefaz.reinf.eventos.retorno package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.tti.sefaz.reinf.eventos.retorno
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Reinf }
     * 
     */
    public Reinf createReinf() {
        return new Reinf();
    }

    /**
     * Create an instance of {@link TRegistroOcorrencias }
     * 
     */
    public TRegistroOcorrencias createTRegistroOcorrencias() {
        return new TRegistroOcorrencias();
    }

    /**
     * Create an instance of {@link Reinf.RetornoLoteEventos }
     * 
     */
    public Reinf.RetornoLoteEventos createReinfRetornoLoteEventos() {
        return new Reinf.RetornoLoteEventos();
    }

    /**
     * Create an instance of {@link TStatus }
     * 
     */
    public TStatus createTStatus() {
        return new TStatus();
    }

    /**
     * Create an instance of {@link TIdeTransmissor }
     * 
     */
    public TIdeTransmissor createTIdeTransmissor() {
        return new TIdeTransmissor();
    }

    /**
     * Create an instance of {@link TArquivoReinf }
     * 
     */
    public TArquivoReinf createTArquivoReinf() {
        return new TArquivoReinf();
    }

    /**
     * Create an instance of {@link TRegistroOcorrencias.Ocorrencias }
     * 
     */
    public TRegistroOcorrencias.Ocorrencias createTRegistroOcorrenciasOcorrencias() {
        return new TRegistroOcorrencias.Ocorrencias();
    }

    /**
     * Create an instance of {@link Reinf.RetornoLoteEventos.RetornoEventos }
     * 
     */
    public Reinf.RetornoLoteEventos.RetornoEventos createReinfRetornoLoteEventosRetornoEventos() {
        return new Reinf.RetornoLoteEventos.RetornoEventos();
    }

}
