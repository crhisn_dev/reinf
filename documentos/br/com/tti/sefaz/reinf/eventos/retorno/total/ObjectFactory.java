//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.09.24 �s 01:35:49 PM BRT 
//


package br.com.tti.sefaz.reinf.eventos.retorno.total;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.tti.sefaz.reinf.eventos.retorno.total package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.tti.sefaz.reinf.eventos.retorno.total
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Reinf }
     * 
     */
    public Reinf createReinf() {
        return new Reinf();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotal }
     * 
     */
    public Reinf.EvtTotal createReinfEvtTotal() {
        return new Reinf.EvtTotal();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotal.InfoTotal }
     * 
     */
    public Reinf.EvtTotal.InfoTotal createReinfEvtTotalInfoTotal() {
        return new Reinf.EvtTotal.InfoTotal();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotal.InfoTotal.IdeEstab }
     * 
     */
    public Reinf.EvtTotal.InfoTotal.IdeEstab createReinfEvtTotalInfoTotalIdeEstab() {
        return new Reinf.EvtTotal.InfoTotal.IdeEstab();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotal.InfoTotal.IdeEstab.RTom }
     * 
     */
    public Reinf.EvtTotal.InfoTotal.IdeEstab.RTom createReinfEvtTotalInfoTotalIdeEstabRTom() {
        return new Reinf.EvtTotal.InfoTotal.IdeEstab.RTom();
    }

    /**
     * Create an instance of {@link TRegistroOcorrencias }
     * 
     */
    public TRegistroOcorrencias createTRegistroOcorrencias() {
        return new TRegistroOcorrencias();
    }

    /**
     * Create an instance of {@link TStatus }
     * 
     */
    public TStatus createTStatus() {
        return new TStatus();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotal.IdeEvento }
     * 
     */
    public Reinf.EvtTotal.IdeEvento createReinfEvtTotalIdeEvento() {
        return new Reinf.EvtTotal.IdeEvento();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotal.IdeContri }
     * 
     */
    public Reinf.EvtTotal.IdeContri createReinfEvtTotalIdeContri() {
        return new Reinf.EvtTotal.IdeContri();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotal.IdeRecRetorno }
     * 
     */
    public Reinf.EvtTotal.IdeRecRetorno createReinfEvtTotalIdeRecRetorno() {
        return new Reinf.EvtTotal.IdeRecRetorno();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotal.InfoRecEv }
     * 
     */
    public Reinf.EvtTotal.InfoRecEv createReinfEvtTotalInfoRecEv() {
        return new Reinf.EvtTotal.InfoRecEv();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotal.InfoTotal.IdeEstab.RPrest }
     * 
     */
    public Reinf.EvtTotal.InfoTotal.IdeEstab.RPrest createReinfEvtTotalInfoTotalIdeEstabRPrest() {
        return new Reinf.EvtTotal.InfoTotal.IdeEstab.RPrest();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotal.InfoTotal.IdeEstab.RRecRepAD }
     * 
     */
    public Reinf.EvtTotal.InfoTotal.IdeEstab.RRecRepAD createReinfEvtTotalInfoTotalIdeEstabRRecRepAD() {
        return new Reinf.EvtTotal.InfoTotal.IdeEstab.RRecRepAD();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotal.InfoTotal.IdeEstab.RComl }
     * 
     */
    public Reinf.EvtTotal.InfoTotal.IdeEstab.RComl createReinfEvtTotalInfoTotalIdeEstabRComl() {
        return new Reinf.EvtTotal.InfoTotal.IdeEstab.RComl();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotal.InfoTotal.IdeEstab.RCPRB }
     * 
     */
    public Reinf.EvtTotal.InfoTotal.IdeEstab.RCPRB createReinfEvtTotalInfoTotalIdeEstabRCPRB() {
        return new Reinf.EvtTotal.InfoTotal.IdeEstab.RCPRB();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotal.InfoTotal.IdeEstab.RRecEspetDesp }
     * 
     */
    public Reinf.EvtTotal.InfoTotal.IdeEstab.RRecEspetDesp createReinfEvtTotalInfoTotalIdeEstabRRecEspetDesp() {
        return new Reinf.EvtTotal.InfoTotal.IdeEstab.RRecEspetDesp();
    }

    /**
     * Create an instance of {@link Reinf.EvtTotal.InfoTotal.IdeEstab.RTom.InfoCRTom }
     * 
     */
    public Reinf.EvtTotal.InfoTotal.IdeEstab.RTom.InfoCRTom createReinfEvtTotalInfoTotalIdeEstabRTomInfoCRTom() {
        return new Reinf.EvtTotal.InfoTotal.IdeEstab.RTom.InfoCRTom();
    }

}
