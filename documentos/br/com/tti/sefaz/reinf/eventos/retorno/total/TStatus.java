//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa��o de Refer�ncia (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica��es neste arquivo ser�o perdidas ap�s a recompila��o do esquema de origem. 
// Gerado em: 2018.09.24 �s 01:35:49 PM BRT 
//


package br.com.tti.sefaz.reinf.eventos.retorno.total;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de TStatus complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cdRetorno">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}unsignedByte">
 *               &lt;pattern value="[0|1|2]"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="descRetorno">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="1000"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="regOcorrs" type="{http://www.reinf.esocial.gov.br/schemas/evtTotal/v1_04_00}TRegistroOcorrencias" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TStatus", propOrder = {
    "cdRetorno",
    "descRetorno",
    "regOcorrs"
})
public class TStatus {

    protected short cdRetorno;
    @XmlElement(required = true)
    protected String descRetorno;
    protected List<TRegistroOcorrencias> regOcorrs;

    /**
     * Obt�m o valor da propriedade cdRetorno.
     * 
     */
    public short getCdRetorno() {
        return cdRetorno;
    }

    /**
     * Define o valor da propriedade cdRetorno.
     * 
     */
    public void setCdRetorno(short value) {
        this.cdRetorno = value;
    }

    /**
     * Obt�m o valor da propriedade descRetorno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescRetorno() {
        return descRetorno;
    }

    /**
     * Define o valor da propriedade descRetorno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescRetorno(String value) {
        this.descRetorno = value;
    }

    /**
     * Gets the value of the regOcorrs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regOcorrs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegOcorrs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TRegistroOcorrencias }
     * 
     * 
     */
    public List<TRegistroOcorrencias> getRegOcorrs() {
        if (regOcorrs == null) {
            regOcorrs = new ArrayList<TRegistroOcorrencias>();
        }
        return this.regOcorrs;
    }

}
